package it.unibo.mymap.landmark;

import android.content.Context;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import it.unibo.mymap.MainActivity;
import it.unibo.mymap.database.Landmark;
import it.unibo.mymap.R;
import it.unibo.mymap.utilities.Utility;

public class LandmarkCardMenu {
    private final PopupMenu menu;
    public LandmarkCardMenu(final Context context, View anchor, final Landmark l) {
        menu = new PopupMenu(context, anchor);
        final LandmarkViewModel viewModel = new ViewModelProvider((ViewModelStoreOwner)
                context).get(LandmarkViewModel.class);
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.edit_landmark:
                        ((MainActivity)context).startActivityForResult(new Intent(context, EditLandmarkActivity.class)
                                .putExtra("id_landmark", l.getId()), Utility.EDIT_LANDMARK);
                        return true;
                    case R.id.delete_landmark:
                        viewModel.deleteLandmark(l);
                        return true;
                    default:
                        return false;
                }
            }
        });
        menu.getMenuInflater().inflate(R.menu.landmark_card_menu, menu.getMenu());
    }

    public void openMenu(){
        menu.show();
    }



}
