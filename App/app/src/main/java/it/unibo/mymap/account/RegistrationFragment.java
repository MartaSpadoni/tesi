package it.unibo.mymap.account;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import it.unibo.mymap.R;
import it.unibo.mymap.utilities.Utility;

import static it.unibo.mymap.utilities.Utility.inputNotEmpty;
import static it.unibo.mymap.utilities.Utility.replaceFragment;

public class RegistrationFragment extends Fragment {
    private FragmentActivity activity;
    private Bitmap bitmap;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        activity = getActivity();
        final View view = inflater.inflate(R.layout.fragment_registration, container, false);
        if(savedInstanceState != null){
            ((ShapeableImageView)view.findViewById(R.id.imageView)).setImageBitmap(bitmap);
        }
        if(activity != null) {
            final SharedPreferences sharedPreferences = activity.getSharedPreferences(getString(R.string.settings_file), Context.MODE_PRIVATE);
            final SharedPreferences.Editor editor = sharedPreferences.edit();
            final MaterialButton button = view.findViewById(R.id.signUpButton);
            final TextInputEditText nameEditText = view.findViewById(R.id.nameTextInputEditText);
            final TextInputLayout nameInputLayout = view.findViewById(R.id.nameTextInputLayout);
            view.findViewById(R.id.imageView).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utility.takePictureIntent(activity);
                }
            });
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (inputNotEmpty(nameEditText.getText())) {
                        Uri imageUri = ((RegistrationActivity)activity).getCurrentPhotoUri();
                        if(imageUri != null){
                            editor.putString(getString(R.string.accountImage), imageUri.toString());
                        }
                        editor.putString(getString(R.string.username), nameEditText.getText().toString());
                        editor.putBoolean(getString(R.string.first_access), false); // da cambiare in false dopo i test
                        editor.commit();
                        replaceFragment((AppCompatActivity) activity, new ChooseAvatarFragment(), ChooseAvatarFragment.class.getName());
                    } else {
                        nameInputLayout.setError(getString(R.string.insert_your_name));
                    }

                }
            });
        }
        return view;
    }


    /**
     * Method use to set the bitmap that will be displayed on the ImageView
     * @param bitmap bitmap representing the picture taken.
     */
    void setImageView(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

}
