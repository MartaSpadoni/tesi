package it.unibo.mymap.map;

import android.graphics.PointF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;

import com.mapbox.geojson.BoundingBox;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.geojson.Polygon;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.turf.TurfJoins;
import com.mapbox.turf.TurfMeasurement;

import java.util.ArrayList;
import java.util.List;

import it.unibo.mymap.utilities.MapUtility;

import static it.unibo.mymap.map.MapFragment.FREEHAND_DRAW_FILL_LAYER_SOURCE_ID;
import static it.unibo.mymap.map.MapFragment.FREEHAND_DRAW_LINE_LAYER_SOURCE_ID;

public class MapOnTouchListener implements View.OnTouchListener {
    private MapboxMap mapboxMap;
    private List<Point> freehandTouchPointListForLine;
    private List<Point> freehandTouchPointListForPolygon;
    private boolean drawSingleLineOnly;
    private  FeatureCollection landmarks;
    private MapView mapView;
    private MapFragment fragment;

    public MapOnTouchListener(MapboxMap mapboxMap, MapView mapView,
                              FeatureCollection landmarks, MapFragment fragment) {
        this.mapboxMap = mapboxMap;
        this.freehandTouchPointListForLine = new ArrayList<>();
        this.freehandTouchPointListForPolygon = new ArrayList<>();
        this.mapView = mapView;
        this.landmarks = landmarks;
        this.fragment = fragment;
    }

    @Override
    public boolean onTouch(View v, final MotionEvent event) {
        LatLng latLngTouchCoordinate = mapboxMap.getProjection().fromScreenLocation(
                new PointF(event.getX(), event.getY()));

        final Point screenTouchPoint = Point.fromLngLat(latLngTouchCoordinate.getLongitude(),
                latLngTouchCoordinate.getLatitude());

        // Draw the line on the map as the finger is dragged along the map
        freehandTouchPointListForLine.add(screenTouchPoint);
        mapboxMap.getStyle(new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                GeoJsonSource drawLineSource = style.getSourceAs(FREEHAND_DRAW_LINE_LAYER_SOURCE_ID);
                if (drawLineSource != null) {
                    drawLineSource.setGeoJson(LineString.fromLngLats(freehandTouchPointListForLine));
                }

                // Draw a polygon area if drawSingleLineOnly == false
                if (!drawSingleLineOnly) {
                    if (freehandTouchPointListForPolygon.size() < 2) {
                        freehandTouchPointListForPolygon.add(screenTouchPoint);
                    } else if (freehandTouchPointListForPolygon.size() == 2) {
                        freehandTouchPointListForPolygon.add(screenTouchPoint);
                        freehandTouchPointListForPolygon.add(freehandTouchPointListForPolygon.get(0));
                    } else {
                        freehandTouchPointListForPolygon.remove(freehandTouchPointListForPolygon.size() - 1);
                        freehandTouchPointListForPolygon.add(screenTouchPoint);
                        freehandTouchPointListForPolygon.add(freehandTouchPointListForPolygon.get(0));
                    }
                }

                // Create and show a FillLayer polygon where the search area is
                GeoJsonSource fillPolygonSource = style.getSourceAs(FREEHAND_DRAW_FILL_LAYER_SOURCE_ID);
                List<List<Point>> polygonList = new ArrayList<>();
                polygonList.add(freehandTouchPointListForPolygon);
                Polygon drawnPolygon = Polygon.fromLngLats(polygonList);
                if (fillPolygonSource != null) {
                    fillPolygonSource.setGeoJson(drawnPolygon);
                }

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if (!drawSingleLineOnly) {
                        freehandTouchPointListForLine.add(freehandTouchPointListForPolygon.get(0));
                    }
                    FeatureCollection area = FeatureCollection.fromFeature(Feature.fromGeometry(
                            drawnPolygon));
                    BoundingBox bb = MapUtility.fromTurfToMapbox(TurfMeasurement.bbox(drawnPolygon));
                    Log.i("MY", "(" + bb.south() + "," + bb.west() + ","
                            + bb.north() + "," + bb.east() + ")");
                    FeatureCollection pointsInSearchArea = FeatureCollection.fromFeatures(new ArrayList<Feature>());
                    if(landmarks != null) {
                        pointsInSearchArea=TurfJoins.pointsWithinPolygon(landmarks, area);
                    }
                    fragment.onPostDrawing(pointsInSearchArea, area,"(" + bb.south() + "," + bb.west() + ","
                            + bb.north() + "," + bb.east() + ")");
                    mapView.setOnTouchListener(null);
                }
            }
        });
    return true;
    }
}
