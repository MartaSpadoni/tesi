package it.unibo.mymap.map;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.graphhopper.directions.api.client.model.GeocodingLocation;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.LocationComponentOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.maps.SupportMapFragment;
import com.mapbox.mapboxsdk.style.layers.FillLayer;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.unibo.mymap.landmark.AddLandmarkDialog;
import it.unibo.mymap.landmark.LandmarkViewModel;
import it.unibo.mymap.MainActivity;
import it.unibo.mymap.R;
import it.unibo.mymap.database.Landmark;
import it.unibo.mymap.geocoding.FetchGeocodingConfig;
import it.unibo.mymap.geocoding.FetchGeocodingTask;
import it.unibo.mymap.geocoding.FetchGeocodingTaskCallbackInterface;
import it.unibo.mymap.utilities.InternetUtilities;
import it.unibo.mymap.utilities.MapUtility;
import it.unibo.mymap.utilities.Utility;

import static android.content.Context.MODE_PRIVATE;
import static android.os.Looper.getMainLooper;
import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.mapbox.mapboxsdk.style.expressions.Expression.eq;
import static com.mapbox.mapboxsdk.style.expressions.Expression.get;
import static com.mapbox.mapboxsdk.style.expressions.Expression.literal;
import static com.mapbox.mapboxsdk.style.layers.Property.ICON_ANCHOR_BOTTOM;
import static com.mapbox.mapboxsdk.style.layers.Property.LINE_JOIN_ROUND;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAnchor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconOffset;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineJoin;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;
import static it.unibo.mymap.utilities.MapUtility.GC_CITY;
import static it.unibo.mymap.utilities.MapUtility.GC_COUNRTY;
import static it.unibo.mymap.utilities.MapUtility.GC_HN;
import static it.unibo.mymap.utilities.MapUtility.GC_STREET;
import static it.unibo.mymap.utilities.MapUtility.OP_NAME;
import static it.unibo.mymap.utilities.MapUtility.findLandmarks;
import static it.unibo.mymap.utilities.MapUtility.getLandmarkFeature;
import static it.unibo.mymap.utilities.MapUtility.getLatLongFromLocation;

public class MapFragment extends Fragment implements AddLandmarkDialog.AddLandmarkDialogListener, MapboxMap.OnMapClickListener,
        FetchGeocodingTaskCallbackInterface, APIRequestManager.APICallback, PoiDialog.PoiDialogListener, PathComponent.RouteListener {

    private static final String GEOJSON_SOURCE_ID = "GEOJSON_SOURCE_ID";
    private static final String MARKER_IMAGE_ID = "MARKER_IMAGE_ID";
    private static final String MARKER_LAYER_ID = "MARKER_LAYER_ID";
    private static final String INFO_LAYER_ID = "INFO_LAYER_ID";

    private static final String GEOCODING_LAYER_ID = "GEOCODING_LAYER_ID";
    private static final String GEO_MARKER_IMG_ID = "GEO_MARKER_IMAGE_ID";
    private static final String GEOCODING_SOURCE_ID = "GEOCODING_SOURCE_ID";
    private static final String GEO_INFO_LAYER_ID = "GEO_INFO_LAYER_ID";

    private static final String SIMPLE_MARKER_SOURCE_ID = "SIMPLE_MARKER_SOURCE:ID";
    private static final String SIMPLE_MARKER_IMAGE_ID = "SIMPLE_MARKER_IMAGE_ID";
    private static final String SIMPLE_LAYER_ID = "SIMPLE_LAYER_ID" ;
    private static final String SIMPLE_INFO_LAYER_ID = "SIMPLE_INFO_ID";

    public static final String FREEHAND_DRAW_LINE_LAYER_SOURCE_ID = "FREEHAND_DRAW_LINE_LAYER_SOURCE_ID";
    public static final String FREEHAND_DRAW_FILL_LAYER_SOURCE_ID = "FREEHAND_DRAW_FILL_LAYER_SOURCE_ID";
    public static final String FREEHAND_DRAW_LINE_LAYER_ID = "FREEHAND_DRAW_LINE_LAYER_ID";
    public static final String FREEHAND_DRAW_FILL_LAYER_ID = "FREEHAND_DRAW_FILL_LAYER_ID";

    private static final long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    private static final long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
    private static final String URI_STYLE = "mapbox://styles/martaspadoni/ckd63nzqu0dfq1in08urgv4tm";
    private static final String LINE_COLOR = "#46830F";
    private static final float LINE_WIDTH = 5f;
    private static final float LINE_OPACITY = 1f;
    private static final float FILL_OPACITY = .4f;

    private LandmarkViewModel viewModel;
    private FragmentActivity activity;

    private Button directionsButton;
    private Button navigationButton;
    private FloatingActionButton resetButton;
    private FloatingActionButton drawButton;


    private MapboxMap mapboxMap;
    private NavigationMapRoute mapRoute;
    private MapView mapView;
    private SupportMapFragment mapFragment;
    private PermissionsManager permissionsManager;
    private LocationEngine locationEngine;
    private MyLocationEngineCallback callback;
    private PathComponent pathComponent;

    private LatLng lastPointLongClick;
    private LatLng lastPointClick;
    private LatLng destination;
    private String destinationName;
    private GeoJsonSource myLandmarkSource;
    //private GeoJsonSource geocodingSource;
    private GeoJsonSource source;
    private GeoJsonSource freeHandLineSource;
    private GeoJsonSource fillSource;
    private List<Point> freehandTouchPointListForLine = new ArrayList<>();
    private List<Point> freehandTouchPointListForPolygon = new ArrayList<>();
    private FeatureCollection featureCollection;
    //private FeatureCollection geocodingFeatures;
    private FeatureCollection features;
    private List<Landmark> landmarkList;
    private DirectionsRoute routeToNavigate;
    private double distance;
    private double duration;
    private boolean create;
    private int selectedLandmark;
    private boolean showAll;
    private ProgressBar loading;
    private APIRequestManager manager;

    private PoiDialog.PoiType poiType;

    public MapFragment(){
        this.selectedLandmark = -1;
    }

    public MapFragment(int selectedLandmark, boolean showAll) {
        this.selectedLandmark = selectedLandmark;
        this.showAll = showAll;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Mapbox.getInstance(getActivity(), getString(R.string.mapbox_access_token));
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_toolbar, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            /**
             * Called when the user submits the query. This could be due to a key press on the keyboard
             * or due to pressing a submit button.
             * @param query the query text that is to be submitted
             * @return true if the query has been handled by the listener, false to let the
             * SearchView perform the default action.
             */
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query != null && !query.isEmpty()) {
                    String currentGeocodingInput = query;
                    String point = null;
                    LatLng pointLatLng = MapFragment.this.mapboxMap.getCameraPosition().target;
                    if (pointLatLng != null)
                        point = pointLatLng.getLatitude() + "," + pointLatLng.getLongitude();
                    showLoading();
                    new FetchGeocodingTask(MapFragment.this, getString(R.string.gh_key), null)
                            .execute(new FetchGeocodingConfig(currentGeocodingInput, "it", 5, false, point, "default"));
                }
                return true;
            }

            /**
             * Called when the query text is changed by the user.
             * @param newText the new content of the query text field.
             * @return false if the SearchView should perform the default action of showing any
             * suggestions if available, true if the action was handled by the listener.
             */
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pathComponent = new PathComponent(getActivity(), this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
        if(activity != null){
            Utility.setUpToolbar((AppCompatActivity) activity, "La mappa");
            InternetUtilities.makeSnackbar(activity);
            if(!InternetUtilities.getIsNetworkConnected()){
                InternetUtilities.getSnackbar().show();
            }

            resetButton = activity.findViewById(R.id.resetFAB);
            drawButton = activity.findViewById(R.id.drawFab);
            loading = activity.findViewById(R.id.progressBar);
            mapView = activity.findViewById(R.id.mapView);

            if(mapView != null) {
                mapView.onCreate(savedInstanceState);
                setButtonOnClickListener();
                setUpMap();
            }
            viewModel = new ViewModelProvider(activity).get(LandmarkViewModel.class);
            viewModel.getLandmarkList().observe(activity, new Observer<List<Landmark>>() {
            @Override
            public void onChanged(List<Landmark> landmarks) {
                if (landmarks != null && !landmarks.isEmpty()) {
                    if(create){
                        setMarkerForLandmark(landmarks.get(0));
                        create = false;
                    }
                    landmarkList = landmarks;
                }
            }
        });
        }
    }



    private void setButtonOnClickListener() {

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetMapSource();
            }
        });

        drawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDrawingElement();
                pathComponent.setComponentVisibility(GONE);
                setLandmarkFeatureCollection(landmarkList);
                if(mapView != null){
                    new PoiDialog(MapFragment.this).show(activity.getSupportFragmentManager(), PoiDialog.class.getName());
                }
            }
        });

        activity.findViewById(R.id.myLocFab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapboxMap.getLocationComponent() != null){
                    /*mapboxMap.easeCamera(CameraUpdateFactory.
                            newLatLng(new LatLng(getUserOriginPoint().latitude(), getUserOriginPoint().longitude())));*/
                    mapboxMap.getLocationComponent().setCameraMode(CameraMode.TRACKING_COMPASS);
                }

            }
        });
    }

    public void showEcoFeedback(){
        new NavigationFeedbackDialog(distance, duration).show(activity.getSupportFragmentManager()
                , NavigationFeedbackDialog.class.getName());
    }

    public void onNavigationEnd(){
        resetMapSource();
    }

    public void setUpMap(){
        if(mapView != null){
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(final MapboxMap mapboxMap) {
                    MapFragment.this.mapboxMap = mapboxMap;
                    MapFragment.this.mapboxMap.setStyle(new Style.Builder()
                            .fromUri(URI_STYLE), new Style.OnStyleLoaded() {
                        @Override
                        public void onStyleLoaded(@NonNull Style style) {

                            enableLocationComponent(style);
                            setUpSourceAndLayer(style);
                            if(mapRoute == null){
                                mapRoute = new NavigationMapRoute(mapView, mapboxMap);
                            }
                            showLandmarks();
                            addMapClickListeners();

                        }
                    });
                }
            });
        }
    }

    private void addMapClickListeners(){
        mapboxMap.addOnMapLongClickListener(new MapboxMap.OnMapLongClickListener() {
            @Override
            public boolean onMapLongClick(@NonNull LatLng point) {
                lastPointLongClick = point;
                DialogFragment addLandmarkDialog = new AddLandmarkDialog();
                addLandmarkDialog.show(activity.getSupportFragmentManager(), AddLandmarkDialog.class.getName());
                return false;
            }
        });

        mapboxMap.addOnMapClickListener(MapFragment.this);
    }

    @SuppressLint("MissingPermission")
    private void enableLocationComponent(Style loadedMapStyle){
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(activity)) {
            SharedPreferences sp = activity.getSharedPreferences(getString(R.string.settings_file), MODE_PRIVATE);
            int avatar = Utility.getAvatarDrawable(sp.getString(getString(R.string.avatar_path), ""));
            LocationComponentOptions customLocationComponentOpt =
                    LocationComponentOptions.builder(activity).foregroundDrawable(avatar)
                            .foregroundDrawableStale(avatar).compassAnimationEnabled(true).build();
            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(
                    LocationComponentActivationOptions.builder(activity, loadedMapStyle)
                            .locationComponentOptions(customLocationComponentOpt)
                            .useDefaultLocationEngine(false)
                            .build());
            // Enable the LocationComponent so that it's actually visible on the map
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING_COMPASS);
            locationComponent.setRenderMode(RenderMode.COMPASS);
            initLocationEngine();
        } else {
            ((MainActivity)activity).requestLocationPermission();
        }
    }

    @SuppressLint("MissingPermission")
    private void initLocationEngine() {
        callback = new MyLocationEngineCallback(activity);
        locationEngine = LocationEngineProvider.getBestLocationEngine(activity);

        LocationEngineRequest request = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();

        locationEngine.requestLocationUpdates(request, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }

    public void updateLocationComponent(Location location){
        if(mapboxMap != null && mapboxMap.getLocationComponent() != null) {
            this.mapboxMap.getLocationComponent().forceLocationUpdate(location);
        }
    }

    private void showLandmarks(){
        if(selectedLandmark > -1 && landmarkList != null){
            destination = new LatLng(landmarkList.get(selectedLandmark).getLat(), landmarkList.get(selectedLandmark).getLng());
            destinationName = landmarkList.get(selectedLandmark).getName();
            setViewOnNewDestination();
            setMarkerForLandmark(landmarkList.get(selectedLandmark));
            MapFragment.this.mapboxMap.easeCamera(CameraUpdateFactory.newLatLng(destination), 2000);
            selectedLandmark = -1;
        }else if(showAll && landmarkList != null){
            setMarkersForLandmarkList(landmarkList);
            showAll = false;
        }
    }

    public void onPostDrawing(FeatureCollection landmarksInArea, FeatureCollection area, String bbox){
        switch (poiType){
            case MYLANDMARK:
                if(!landmarksInArea.features().isEmpty()) {
                    generateLandmarksView(findLandmarks(landmarkList, landmarksInArea));
                }else{
                    featureCollection = FeatureCollection.fromFeatures(new ArrayList<Feature>());
                    myLandmarkSource.setGeoJson(featureCollection);
                    Utility.makeNoResultToast(activity);
                }
                break;
            case TOURISM:
                showLoading();
                manager = new APIRequestManager(activity, area, this);
                manager.findTourismFeature(bbox);
                break;
            case FOUNTAINS:
                showLoading();
                manager = new APIRequestManager(activity,area,this);
                manager.findDrinkingFountains(bbox);
                break;
            case LIBRARY:
                showLoading();
                manager = new APIRequestManager(activity, area, this);
                manager.findLibrary(bbox);
        }

    }

    private void setMarkerForLandmark(Landmark l){
        featureCollection = FeatureCollection.fromFeature(getLandmarkFeature(l));
        generateLandmarksView(featureCollection);
    }

    private void setLandmarkFeatureCollection(List<Landmark> list){
        List<Feature> featureList = new ArrayList<>();
        if(list != null) {
            for (Landmark l : list) {
                featureList.add(getLandmarkFeature(l));
            }
            featureCollection = FeatureCollection.fromFeatures(featureList);
        }
    }

    private void generateLandmarksView(FeatureCollection featureCollection){
        this.featureCollection = featureCollection;
        new MapUtility.GenerateViewIconTask((MainActivity) activity,
                true, MapUtility.InfoWindowType.LANDMARK).execute(featureCollection);
    }

    private void setMarkersForLandmarkList(List<Landmark> list){
        setLandmarkFeatureCollection(list);
        generateLandmarksView(featureCollection);
    }

    /**
     * Updates the display of data on the map after the FeatureCollection has been modified
     */
    public void refreshSource() {
        if (myLandmarkSource != null && featureCollection != null) {
            myLandmarkSource.setGeoJson(featureCollection);
        }
    }

    /**
     * Invoked when the bitmaps have been generated from a view.
     */
    public void setImageGenResults(final HashMap<String, Bitmap> imageMap) {
        if (mapboxMap != null) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    Log.i("MY", "setto le immagini");
                    if(imageMap != null) {
                        style.addImages(imageMap);
                    }
                }
            });
        }
    }

    private void setUpSourceAndLayer(Style style) {
        myLandmarkSource = new GeoJsonSource(GEOJSON_SOURCE_ID);
        style.addSource(myLandmarkSource);
        style.addImage(MARKER_IMAGE_ID, BitmapFactory.decodeResource(
                this.getResources(), R.drawable.green_marker));
        style.addLayer(new SymbolLayer(MARKER_LAYER_ID, GEOJSON_SOURCE_ID)
                .withProperties(
                        iconImage(MARKER_IMAGE_ID),
                        iconAllowOverlap(true),
                        iconIgnorePlacement(true),
                        iconSize(0.4f)
                ));
        style.addLayer(new SymbolLayer(INFO_LAYER_ID, GEOJSON_SOURCE_ID)
                .withProperties(
                        /* show image with id title based on the value of the name feature property */
                        iconImage("{name}"),
                        iconAnchor(ICON_ANCHOR_BOTTOM),
                        /* all info window and marker image to appear at the same time*/
                        iconAllowOverlap(true),
                        iconOffset(new Float[] {-2f, -28f})
                )
                /* add a filter to show only when selected feature property is true */
                .withFilter(eq((get(MapUtility.LM_STATE)), literal(true))));

       /* geocodingSource = new GeoJsonSource(GEOCODING_SOURCE_ID);
        style.addSource(geocodingSource);
        style.addImage(GEO_MARKER_IMG_ID, BitmapFactory.decodeResource(
                this.getResources(), R.drawable.red_marker_center));
        style.addLayer(new SymbolLayer(GEOCODING_LAYER_ID, GEOCODING_SOURCE_ID)
                .withProperties(
                        iconImage(GEO_MARKER_IMG_ID),
                        iconAllowOverlap(true),
                        iconIgnorePlacement(true),
                        iconSize(0.4f)
                ));
        style.addLayer(new SymbolLayer(GEO_INFO_LAYER_ID, GEOCODING_SOURCE_ID)
                .withProperties(
                        /* show image with id title based on the value of the name feature property */
                     /*   iconImage("{OSM}"),
                        iconAnchor(ICON_ANCHOR_BOTTOM),
                        /* all info window and marker image to appear at the same time*/
                      /*  iconAllowOverlap(true),
                        iconOffset(new Float[] {-2f, -28f})
                ).withFilter(eq((get(MapUtility.LM_STATE)), literal(true))));*/

        source = new GeoJsonSource(SIMPLE_MARKER_SOURCE_ID);
        style.addSource(source);
        style.addImage(SIMPLE_MARKER_IMAGE_ID, BitmapFactory.decodeResource(
                this.getResources(), R.drawable.yellow_marker));

        style.addLayer(new SymbolLayer(SIMPLE_LAYER_ID, SIMPLE_MARKER_SOURCE_ID)
                .withProperties(
                        iconImage(SIMPLE_MARKER_IMAGE_ID),
                        iconAllowOverlap(true),
                        iconIgnorePlacement(true),
                        iconSize(0.4f)
                ));
        style.addLayer(new SymbolLayer(SIMPLE_INFO_LAYER_ID, SIMPLE_MARKER_SOURCE_ID)
                .withProperties(
                        /* show image with id title based on the value of the name feature property */
                        iconImage("{id}"),
                        iconAnchor(ICON_ANCHOR_BOTTOM),
                        /* all info window and marker image to appear at the same time*/
                        iconAllowOverlap(true),
                        iconOffset(new Float[] {-2f, -28f})
                ).withFilter(eq((get(MapUtility.LM_STATE)), literal(true))));

        freeHandLineSource = new GeoJsonSource(FREEHAND_DRAW_LINE_LAYER_SOURCE_ID);
        style.addSource(freeHandLineSource);
        style.addLayerBelow(new LineLayer(FREEHAND_DRAW_LINE_LAYER_ID,
                FREEHAND_DRAW_LINE_LAYER_SOURCE_ID).withProperties(
                lineWidth(LINE_WIDTH),
                lineJoin(LINE_JOIN_ROUND),
                lineOpacity(LINE_OPACITY),
                lineColor(Color.parseColor(LINE_COLOR))), MARKER_LAYER_ID);

        fillSource = new GeoJsonSource(FREEHAND_DRAW_FILL_LAYER_SOURCE_ID);
        style.addSource(fillSource);
        // Add freehand draw polygon FillLayer to the map
        style.addLayerBelow(new FillLayer(FREEHAND_DRAW_FILL_LAYER_ID,
                FREEHAND_DRAW_FILL_LAYER_SOURCE_ID).withProperties(
                fillColor(Color.parseColor("#d6ffcc")),
                fillOpacity(FILL_OPACITY)), FREEHAND_DRAW_LINE_LAYER_ID);

    }

    @Override
    public void onAddClick(String name, String description, Uri photoUri) {
        double lng = lastPointLongClick.getLongitude();
        double lat = lastPointLongClick.getLatitude();
        create = true;
        pathComponent.setNewDestination(name, lastPointLongClick, getLatLongFromLocation(mapboxMap.getLocationComponent().getLastKnownLocation()));
        pathComponent.setComponentVisibility(VISIBLE);
        resetButton.setVisibility(View.VISIBLE);
        resetDrawingElement();
        features = FeatureCollection.fromFeatures(new ArrayList<Feature>());
        source.setGeoJson(features);
        mapRoute.updateRouteVisibilityTo(false);
        //resetDrawingElement();
        viewModel.addLandmark(new Landmark(photoUri != null? photoUri.toString() : "", lat, lng, name, description));
    }

    private void setViewOnNewDestination(){
        if(mapRoute != null){
            mapRoute.updateRouteVisibilityTo(false);
        }
        resetButton.setVisibility(View.VISIBLE);
        pathComponent.setComponentVisibility(VISIBLE);
        pathComponent.setNewDestination(destinationName,destination,MapUtility.
                getLatLongFromLocation(mapboxMap.getLocationComponent().getLastKnownLocation()));
    }

    private void setDestination(Feature f){
        destination = new LatLng(((Point)f.geometry()).latitude(),
                ((Point)f.geometry()).longitude());
        destinationName = f.hasProperty("name") && !f.getStringProperty("name").isEmpty() ?
                f.getStringProperty("name") : "punto selezionato";
        setViewOnNewDestination();
    }

    private boolean checkClickOnLayer(LatLng point, String LAYER_ID, String id_property,
                                      FeatureCollection featureCollection, GeoJsonSource source){
        List<Feature> features = mapboxMap.queryRenderedFeatures(mapboxMap.getProjection()
                .toScreenLocation(point), LAYER_ID);
        if (!features.isEmpty()) {
            String name = features.get(0).getStringProperty(id_property);
            if (featureCollection != null) {
                List<Feature> featureList = featureCollection.features();
                for (Feature f : featureList) {
                    if (f.hasProperty(id_property) && f.getStringProperty(id_property).equals(name)) {
                        Boolean selected = f.getBooleanProperty(MapUtility.LM_STATE);
                        f.properties().addProperty(MapUtility.LM_STATE, !selected);
                        setDestination(f);
                        mapboxMap.easeCamera(CameraUpdateFactory.newLatLng(new LatLng(((Point)f.geometry()).latitude(),
                                ((Point)f.geometry()).longitude())));
                    }else{
                        f.properties().addProperty(MapUtility.LM_STATE, false);
                    }
                }
                featureCollection = FeatureCollection.fromFeatures(featureList);
                source.setGeoJson(featureCollection);

            }
            return true;
        }
        return false;
    }


    private boolean checkClickOnInfoWindow(LatLng point, String layer, MapUtility.InfoWindowType infoWindowType){
        List<Feature> features = mapboxMap.queryRenderedFeatures(mapboxMap.getProjection()
                .toScreenLocation(point), layer );
        if (!features.isEmpty()) {
            Feature feature = features.get(0);
            lastPointLongClick = new LatLng(((Point)feature.geometry()).latitude(),
                    ((Point)feature.geometry()).longitude());
            setDestination(feature);
            String placeName = "";
            String placeDescription = "";
            switch (infoWindowType){
                case GEOCODING:
                    placeName = feature.getStringProperty(MapUtility.GC_NAME);
                    placeDescription = feature.getStringProperty(GC_STREET) + " " + feature.getStringProperty(GC_HN)
                            + " " + feature.getStringProperty(GC_CITY) + " " + feature.getStringProperty(GC_COUNRTY);
                    break;
                case OVERPASS:
                    placeName = feature.getStringProperty(OP_NAME);
                    break;
            }

            DialogFragment addLandmarkDialog = new AddLandmarkDialog(placeName, placeDescription);
            addLandmarkDialog.show(activity.getSupportFragmentManager(), AddLandmarkDialog.class.getName());
            return true;
        }
        return false;
    }
    private boolean geocodingClick(LatLng point){
        String stringPoint = point.getLatitude() + "," + point.getLongitude();
        showLoading();
        new FetchGeocodingTask(MapFragment.this, getString(R.string.gh_key), point)
                .execute(new FetchGeocodingConfig("", "it", 5, true, stringPoint, "default"));
        return true;
    }
    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        lastPointClick = point;
        // &&
        //                !checkClickOnLayer(point, GEOCODING_LAYER_ID,
        //                        MapUtility.GC_OSM, geocodingFeatures, geocodingSource)
        return !checkClickOnLayer(point, MARKER_LAYER_ID,
                MapUtility.LM_NAME, featureCollection, myLandmarkSource) &&
                !checkClickOnInfoWindow(point, GEO_INFO_LAYER_ID, MapUtility.InfoWindowType.GEOCODING ) &&
                !checkClickOnLayer(point, INFO_LAYER_ID, MapUtility.LM_NAME, featureCollection, myLandmarkSource) &&
                !checkClickOnInfoWindow(point, SIMPLE_INFO_LAYER_ID, MapUtility.InfoWindowType.OVERPASS)&&
                !checkClickOnLayer(point,SIMPLE_LAYER_ID, "id",
                        features, source ) && drawButton.isShown()? geocodingClick(point) : true;
    }
    private void resetDrawingElement(){
        freeHandLineSource.setGeoJson(FeatureCollection.fromFeatures(new ArrayList<Feature>()));
        fillSource.setGeoJson(FeatureCollection.fromFeatures(new ArrayList<Feature>()));
        drawButton.setVisibility(View.VISIBLE);
    }

    private boolean resetMapSource(){
        boolean v = !resetButton.isShown();
        resetDrawingElement();
        mapView.setOnTouchListener(null);
        if(manager != null){
            manager.cancelRequest();
        }
        hideLoading();
        features = FeatureCollection.fromFeatures(new ArrayList<Feature>());
        source.setGeoJson(features);
        pathComponent.setComponentVisibility(GONE);
        resetButton.setVisibility(View.GONE);
        if(mapRoute != null){
            mapRoute.updateRouteVisibilityTo(false);
        }
        if((featureCollection != null && !featureCollection.features().isEmpty()))/*||
                (geocodingFeatures != null && !geocodingFeatures.features().isEmpty()))*/ {
            featureCollection = FeatureCollection.fromFeatures(new ArrayList<Feature>());
            myLandmarkSource.setGeoJson(featureCollection);
            //geocodingFeatures = FeatureCollection.fromFeatures(new ArrayList<Feature>());
            //geocodingSource.setGeoJson(geocodingFeatures);
        }
        return v;
    }

    public boolean onBackPressed(){
       return resetMapSource();
    }

    private String getOrDefault(String value){
        return value == null ? "" : value;
    }

    private Feature createFeatureFromGeocodingLocation(GeocodingLocation location){
        Feature f = Feature.fromGeometry(Point.fromLngLat(location.getPoint().getLng(), location.getPoint().getLat()));
        f.addStringProperty(MapUtility.GC_OSM, getOrDefault(location.getOsmId()));
        f.addStringProperty(MapUtility.GC_NAME, getOrDefault(location.getName()));
        f.addStringProperty(GC_CITY, getOrDefault(location.getCity()));
        f.addStringProperty(GC_STREET, getOrDefault(location.getStreet()));
        f.addStringProperty(GC_COUNRTY, getOrDefault(location.getCountry()));
        f.addStringProperty(GC_HN, getOrDefault(location.getHousenumber()));
        f.addBooleanProperty(MapUtility.LM_STATE, false);
        return f;
    }

    @Override
    public void onError(String errore) {
        Toast.makeText(activity, errore, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPostExecuteGeocodingSearch(List<GeocodingLocation> points) {
        Log.i("MY", "GECODING FATTO");
        hideLoading();
        if(points != null && !points.isEmpty()){
            final List<Feature> featureList = new ArrayList<>();
            final List<LatLng> pList = new ArrayList<>();
            GeocodingLocation location = points.get(0);
            Feature f = createFeatureFromGeocodingLocation(location);
            setDestination(f);
            featureList.add(f);
            pList.add(new LatLng(location.getPoint().getLat(), location.getPoint().getLng()));

            features = FeatureCollection.fromFeatures(featureList);
            source.setGeoJson(features);
            new MapUtility.GenerateViewIconTask((MainActivity) activity, true, MapUtility.InfoWindowType.GEOCODING)
                    .execute(features);
            if(pList.size() > 1) {
                CameraPosition cameraPosition = this.mapboxMap.getCameraForLatLngBounds(new LatLngBounds.Builder()
                        .includes(pList).build(), new int[]{50, 50, 50, 50});
                this.mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }else{
                this.mapboxMap.animateCamera(CameraUpdateFactory.newLatLng(pList.get(0)));
            }
        }else{

            destination = lastPointClick;
            destinationName = "punto selezionato";
            setViewOnNewDestination();
            features = FeatureCollection.fromFeature(Feature.
                    fromGeometry(Point.fromLngLat(lastPointClick.getLongitude(), lastPointClick.getLatitude())));
            source.setGeoJson(features);
            this.mapboxMap.animateCamera(CameraUpdateFactory.newLatLng(lastPointClick));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        if(locationEngine != null && callback != null) {
            this.locationEngine.removeLocationUpdates(callback);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }


    @Override
    public void onApiResponse(FeatureCollection fc) {
        manager = null;
        features = fc;
        source.setGeoJson(fc);
        hideLoading();
        if(fc.features() != null && !fc.features().isEmpty()){
            new MapUtility.GenerateViewIconTask((MainActivity) activity,
                    false, MapUtility.InfoWindowType.OVERPASS).execute(fc);
        }else{
            Utility.makeNoResultToast(activity);
        }
    }

    public void hideLoading() {
        Log.i("MY", "Hide");
        if(loading.getVisibility() == View.VISIBLE)
        loading.setVisibility(View.INVISIBLE);
    }

    public void showLoading() {
        Log.i("MY", "SHOW");
        if(loading.getVisibility() == View.INVISIBLE)
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    @Override
    public void onChoice(PoiDialog.PoiType poiType) {
        drawButton.setVisibility(View.INVISIBLE);
        resetButton.setVisibility(View.VISIBLE);
        this.poiType = poiType;
        mapView.setOnTouchListener(
                new MapOnTouchListener(mapboxMap, mapView,featureCollection, MapFragment.this));

    }

    @Override
    public void onNewRoute(DirectionsRoute route) {
        Log.i("MY", "setto le route " + route.distance());
        mapRoute.addRoute(route);
        routeToNavigate = route;
        duration = routeToNavigate.duration();
        distance = routeToNavigate.distance();
    }
}
