package it.unibo.mymap.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface LandmarkDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addLandmark(Landmark l);

    @Transaction
    @Query("SELECT * from Landmark ORDER BY id DESC")
    LiveData<List<Landmark>> getLandmarks();

    @Transaction
    @Query("SELECT * from Landmark WHERE id LIKE :id")
    Landmark getLandmark(int id);

    @Update
    void updateLandmark(Landmark l);

    @Delete
    void deleteLandmark(Landmark l);
}
