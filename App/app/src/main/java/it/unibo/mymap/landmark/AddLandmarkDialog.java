package it.unibo.mymap.landmark;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.textfield.TextInputEditText;

import it.unibo.mymap.MainActivity;
import it.unibo.mymap.R;
import it.unibo.mymap.map.MapFragment;
import it.unibo.mymap.utilities.Utility;

public class AddLandmarkDialog extends DialogFragment {
    private ImageView imageView;
    private TextInputEditText placeName;
    private String placeNameText;
    private TextInputEditText placeDescription;
    private String placeDescriptionText;
    private Uri photoUri;
    private AddLandmarkDialogListener listener;

    public AddLandmarkDialog() {
    }

    public AddLandmarkDialog(String placeName, String placeDescription) {
        this.placeNameText = placeName;
        this.placeDescriptionText = placeDescription;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        Log.i("MY", "creato dialog");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_add_landmark, null);
        setUpView(view);
        return builder.setView(view)
                .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    listener.onAddClick(placeName.getText().toString(), placeDescription.getText().toString(), photoUri);
            }
        }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create();
    }

    private void setUpView(View view){
        imageView = view.findViewById(R.id.landmarkImageView);
        placeName = view.findViewById(R.id.placeNameEditText);
        if(placeNameText != null){
            placeName.setText(placeNameText);
        }
        placeDescription = view.findViewById(R.id.placeDescriptionEditText);
        if(placeDescriptionText != null){
            placeDescription.setText(placeDescriptionText);
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.takePictureIntent(getActivity());
            }
        });
        placeName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                AlertDialog d = (AlertDialog) getDialog();
                if(d!=null){
                    d.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(Utility.inputNotEmpty(s));
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        AlertDialog d = (AlertDialog) getDialog();
        if(d!=null && placeName != null && !Utility.inputNotEmpty(placeName.getText())){
            d.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            MapFragment fragmentListener = (MapFragment) ((MainActivity)context).getSupportFragmentManager()
                    .findFragmentByTag(MapFragment.class.getName());
            listener = (AddLandmarkDialogListener)fragmentListener;
        }catch (ClassCastException e){
            Log.i("MY", "wrong context");
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i("MY", "on Dialog Create View");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public  void setImageView(Bitmap bitmap){
        imageView.setImageBitmap(bitmap);
    }


    public void setPhotoUri(Uri photoUri) {
        this.photoUri = photoUri;
    }

    public interface AddLandmarkDialogListener {
        void onAddClick(String name, String description, Uri photoUri);
    }
}

