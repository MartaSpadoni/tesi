package it.unibo.mymap.map;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import it.unibo.mymap.R;

import static java.lang.Math.round;

public class NavigationFeedbackDialog extends DialogFragment {

    //dato: https://www.co2nnect.org/help_sheets/?op_id=602&amp;opt_id=98
    //valore medio emissioni g/m
    private final static double CO2_EMISSION_COEF = 0.133;

    private String distance;
    private String duration;
    private double emission;

    public NavigationFeedbackDialog(double distance, double duration) {
        this.distance = getDistanceFormatted(distance);
        this.duration = DateUtils.formatElapsedTime((long) duration);
        this.emission = distance * CO2_EMISSION_COEF;
    }

    private String getDistanceFormatted(double distance) {
       return distance < 1000 ? String.format("%.2f m", distance) : String.format("%.2f km",distance/1000);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.eco_feedback_dialog, null);
        setUpView(view);
        return builder.setView(view).setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create();
    }

    private Object[] getC02emissionArgs(){
        return emission < 1000 ? new Object[] {emission, "grammi"} : new Object[] {emission/1000, "kg"};
    }

    @SuppressLint("StringFormatMatches")
    private void setUpView(View view) {
        ((TextView)view.findViewById(R.id.distanceTextView)).setText(distance);
        ((TextView)view.findViewById(R.id.durationTextView)).setText(duration);
        ((TextView)view.findViewById(R.id.emissionTextView))
                .setText(getString(R.string.emission_text, getC02emissionArgs()));
    }
}
