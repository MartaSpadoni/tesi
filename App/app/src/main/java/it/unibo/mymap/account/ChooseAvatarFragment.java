package it.unibo.mymap.account;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.material.imageview.ShapeableImageView;

import javax.xml.transform.Templates;

import it.unibo.mymap.R;

public class ChooseAvatarFragment extends Fragment {
    private FragmentActivity activity;
    private String avatarPath;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view =  inflater.inflate(R.layout.fragment_choose_avatar, container, false);
        activity = getActivity();
        if(activity != null) {

            final SharedPreferences sharedPreferences = activity.getSharedPreferences(getString(R.string.settings_file), Context.MODE_PRIVATE);
            final ShapeableImageView maleAvatar = view.findViewById(R.id.maleAvatarImageView);
            final ShapeableImageView femaleAvatar = view.findViewById(R.id.femaleAvatarImageView);
            final TextView instructionTextView = view.findViewById(R.id.instructionTextView);
            final String username = sharedPreferences.getString(getString(R.string.username), "");

            instructionTextView.setText(String.format("%s %s", username, instructionTextView.getText()));
            maleAvatar.setOnClickListener(getAvatarClickListener(maleAvatar, femaleAvatar, "boy_"));
            femaleAvatar.setOnClickListener(getAvatarClickListener(femaleAvatar, maleAvatar, "girl_"));
            view.findViewById(R.id.selectButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(avatarPath != null){
                        final SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(getString(R.string.avatar_path), avatarPath);
                        editor.commit();
                        activity.setResult(Activity.RESULT_OK);
                        activity.finish();
                    }
                }
            });
        }
        return view;
    }

    private View.OnClickListener getAvatarClickListener(final ShapeableImageView selected, final ShapeableImageView other, final String avatarPath){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected.setStrokeColorResource(R.color.colorPrimary);
                selected.setStrokeWidth(20);
                other.setStrokeColorResource(R.color.material_on_surface_emphasis_medium);
                other.setStrokeWidth(2);
                setAvatarPath(avatarPath);
            }
        };
    }

    private void setAvatarPath(String path){
        avatarPath = path;
    }
}
