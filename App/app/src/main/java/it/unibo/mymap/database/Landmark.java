package it.unibo.mymap.database;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Landmark {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String imagePath;
    private double lat;
    private double lng;
    private String name;
    private String description;

    public Landmark(String imagePath, double lat, double lng, String name, String description) {
        this.imagePath = imagePath;
        this.lat = lat;
        this.lng = lng;
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
