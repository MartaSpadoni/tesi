package it.unibo.mymap.landmarkRecyclerView;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;

import org.w3c.dom.Text;

import it.unibo.mymap.R;

public class LandmarkCardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ShapeableImageView thumbnail;
    private TextView title;
    private TextView description;
    private OnItemListener listener;
    private ImageButton menuButton;

    public LandmarkCardViewHolder(@NonNull View itemView, OnItemListener itemListener) {
        super(itemView);

        thumbnail = itemView.findViewById(R.id.landmarkCardImage);
        title = itemView.findViewById(R.id.cardTitleTextView);
        description = itemView.findViewById(R.id.cardDescTextView);
        menuButton = itemView.findViewById(R.id.cardMenuButton);
        listener = itemListener;

        itemView.setOnClickListener(this);
    }

    public ShapeableImageView getThumbnail() {
        return thumbnail;
    }

    public ImageButton getMenuButton(){
        return menuButton;
    }

    public TextView getTitle() {
        return title;
    }

    public TextView getDescription() {
        return description;
    }

    @Override
    public void onClick(View v) {
        listener.onItemClick(getAdapterPosition());
    }
}
