package it.unibo.mymap.account;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.imageview.ShapeableImageView;

import it.unibo.mymap.R;
import it.unibo.mymap.utilities.Utility;

public class AccountFragment extends Fragment {
    private Activity activity;
    private View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_account, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
        if(activity != null){
            Utility.setUpToolbar((AppCompatActivity) activity, getString(R.string.account));
            final SharedPreferences sharedPreferences = activity.getSharedPreferences(getString(R.string.settings_file), Context.MODE_PRIVATE);
            ((TextView)view.findViewById(R.id.accountTextView)).setText(sharedPreferences.getString(getString(R.string.username), ""));
            ((ShapeableImageView)view.findViewById(R.id.imageView))
                    .setImageBitmap(Utility.getImageBitmap(activity,
                            Uri.parse(sharedPreferences.getString(getString(R.string.accountImage), "ic_drawable"))));
            setAvatarImage(sharedPreferences);
            view.findViewById(R.id.changeAvatarButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.startActivityForResult(new Intent(activity, ChangeAvatarActivity.class), Utility.CHANGE_AVATAR);
                }
            });

        }
    }

    public void setAvatarImage(SharedPreferences sharedPreferences){
        ((ImageView)view.findViewById(R.id.avatarBackImageView)).setImageBitmap(BitmapFactory.decodeResource(getResources(),
                Utility.getAvatarDrawable(sharedPreferences.getString(getString(R.string.avatar_path), ""))));
        ((ImageView)view.findViewById(R.id.avatarFrontimageView)).setImageBitmap(BitmapFactory.decodeResource(getResources(),
                Utility.getFrontAvatarDrawable(sharedPreferences.getString(getString(R.string.avatar_path), ""))));
    }
}
