package it.unibo.mymap.landmarkRecyclerView;

import android.app.Activity;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import it.unibo.mymap.landmark.LandmarkCardMenu;
import it.unibo.mymap.database.Landmark;
import it.unibo.mymap.R;
import it.unibo.mymap.utilities.Utility;

public class LandmarkCardAdapter extends RecyclerView.Adapter<LandmarkCardViewHolder> implements Filterable {

    private OnItemListener listener;
    private Activity activity;

    private List<Landmark> landmarkList = new ArrayList<>();
    private List<Landmark> fullList = new ArrayList<>();

    public LandmarkCardAdapter(OnItemListener listener, Activity activity) {
        this.listener = listener;
        this.activity = activity;
    }

    @NonNull
    @Override
    public LandmarkCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.landmark_card_layout, parent, false);

        return new LandmarkCardViewHolder(layoutView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull LandmarkCardViewHolder holder, int position) {
        final Landmark currentLandmark = this.landmarkList.get(position);
        if(!currentLandmark.getImagePath().equals("")){
            holder.getThumbnail().setImageBitmap(Utility.getImageBitmap(activity,
                    Uri.parse(currentLandmark.getImagePath())));
        }

        holder.getTitle().setText(currentLandmark.getName());
        holder.getDescription().setText(currentLandmark.getDescription());
        holder.getMenuButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LandmarkCardMenu(activity, v, currentLandmark).openMenu();
            }
        });
    }

    @Override
    public int getItemCount() {
        return landmarkList.size();
    }

    public void setData(List<Landmark> landmarks){
        this.landmarkList.clear();
        this.landmarkList.addAll(landmarks);
        this.fullList.clear();
        this.fullList.addAll(landmarks);
        notifyDataSetChanged();
    }

    public void updateList(){
        notifyDataSetChanged();
    }

    public int getAbsolutPosition(int relativeIndex){
        Landmark l = this.landmarkList.get(relativeIndex);
        return this.fullList.indexOf(l);
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<Landmark> filteredList = new ArrayList<>();

                //if you have no constraint --> return the full list
                if (constraint == null || constraint.length() == 0) {
                    filteredList.addAll(fullList);
                } else {
                    //else apply the filter and return a filtered list
                    String filterPattern = constraint.toString().toLowerCase().trim();

                    for (Landmark item : fullList) {
                        if (item.getDescription().toLowerCase().contains(filterPattern) ||
                                item.getName().toLowerCase().contains(filterPattern)) {
                            filteredList.add(item);
                        }
                    }
                }

                FilterResults results = new FilterResults();
                results.values = filteredList;

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                landmarkList.clear();
                landmarkList.addAll(((List<Landmark>)results.values));
                notifyDataSetChanged();
            }
        };
    }
}
