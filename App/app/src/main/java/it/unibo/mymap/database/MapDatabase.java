package it.unibo.mymap.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Landmark.class}, version = 1, exportSchema = false)
abstract class MapDatabase extends RoomDatabase {
    abstract LandmarkDAO landmarkDAO();

    //Singleton instance
    private static volatile MapDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;

    //ExecutorService with a fixed thread pool that you will use to run database operations
    // asynchronously on a background thread.
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    //get the singleton instance
    static MapDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MapDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MapDatabase.class, "map_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
