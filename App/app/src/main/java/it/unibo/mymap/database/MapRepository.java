package it.unibo.mymap.database;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class MapRepository {
    private LandmarkDAO landmarkDAO;
    private LiveData<List<Landmark>> landmarkList;

    public MapRepository(Application application){
        MapDatabase db = MapDatabase.getDatabase(application);
        landmarkDAO = db.landmarkDAO();
        landmarkList = landmarkDAO.getLandmarks();
    }

    public LiveData<List<Landmark>> getLandmarkList() {
        return landmarkList;
    }

    public Landmark getLandmark(final int id){
        return landmarkDAO.getLandmark(id);
    }

    public void updateLandmark(final Landmark l){
        MapDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                landmarkDAO.updateLandmark(l);
            }
        });
    }

    public void addLandmark(final Landmark l){
        MapDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                landmarkDAO.addLandmark(l);
            }
        });
    }

    public void deleteLandmark(final Landmark l){
        MapDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                landmarkDAO.deleteLandmark(l);
            }
        });
    }

}
