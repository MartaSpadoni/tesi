package it.unibo.mymap.map;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.services.android.navigation.ui.v5.NavigationView;
import com.mapbox.services.android.navigation.ui.v5.NavigationViewOptions;
import com.mapbox.services.android.navigation.ui.v5.OnNavigationReadyCallback;
import com.mapbox.services.android.navigation.ui.v5.listeners.NavigationListener;
import com.mapbox.services.android.navigation.v5.routeprogress.ProgressChangeListener;
import com.mapbox.services.android.navigation.v5.routeprogress.RouteProgress;

import it.unibo.mymap.R;
import it.unibo.mymap.utilities.Utility;

public class NavigationFragment extends Fragment implements OnNavigationReadyCallback, NavigationListener, ProgressChangeListener {

    private NavigationView navigationView;
    private DirectionsRoute directionsRoute;
    private Activity activity;
    private boolean completed;

    public NavigationFragment(DirectionsRoute directionsRoute) {
        this.directionsRoute = directionsRoute;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        activity = getActivity();
        View view = inflater.inflate(R.layout.activity_edit_landmark, container, false);
        if(activity != null){
            SharedPreferences sp = activity.getSharedPreferences(getString(R.string.settings_file), Context.MODE_PRIVATE);
            if(sp.getString(getString(R.string.avatar_path), "").startsWith("boy")){
                view = inflater.inflate(R.layout.fragment_navigation_boy, container, false);
            }else{
                view =  inflater.inflate(R.layout.fragment_navigation_girl, container, false);
            }
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navigationView = view.findViewById(R.id.navigationView);
        navigationView.onCreate(savedInstanceState);
        navigationView.initialize(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        navigationView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        navigationView.onResume();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        navigationView.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            navigationView.onRestoreInstanceState(savedInstanceState);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        navigationView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        navigationView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        navigationView.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        navigationView.onDestroy();
    }

    @Override
    public void onNavigationReady(boolean isRunning) {
        if(directionsRoute != null) {
            NavigationViewOptions options = NavigationViewOptions.builder()
                    .directionsRoute(directionsRoute)
                    .shouldSimulateRoute(false)
                    .navigationListener(NavigationFragment.this)
                    .progressChangeListener(NavigationFragment.this)
                    .build();
            navigationView.startNavigation(options);
        }
    }

    @Override
    public void onCancelNavigation() {
        navigationView.stopNavigation();
        if(completed){
            activity.setResult(Utility.RESULT_NAVIGATION_END);
        }else{
            activity.setResult(Activity.RESULT_CANCELED);
        }
        activity.finish();
    }

    @Override
    public void onNavigationFinished() {
    }

    @Override
    public void onNavigationRunning() {
        Log.i("MY", "NAVIGAZIONE RUNNING");
    }

    @Override
    public void onProgressChange(Location location, RouteProgress routeProgress) {
        if(routeProgress.durationRemaining() == 0 ||
                (routeProgress.voiceInstruction() != null &&
                        routeProgress.voiceInstruction()
                                .getAnnouncement()
                                .equals("Arrivato a destinazione"))) {
            completed = true;
        }
    }
}
