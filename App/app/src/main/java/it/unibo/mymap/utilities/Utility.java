package it.unibo.mymap.utilities;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.Editable;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import it.unibo.mymap.R;

public class Utility {

    public static final int REGISTRATION = 1;
    public static final int REQUEST_IMAGE_CAPTURE = 2;
    public static final int EDIT_LANDMARK = 3;
    public static final int CHANGE_AVATAR = 4;
    public static final int NAVIGATION_SESSION = 10;
    public static final int RESULT_NAVIGATION_END = 6;

    public static boolean inputNotEmpty(@Nullable Editable text) {
        return text != null && text.length() > 0;
    }

    public static void replaceFragment(AppCompatActivity activity, Fragment fragment, String tag){
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment, tag)
                .commit();
    }

    public static void setUpToolbar(AppCompatActivity activity, String title) {
        Toolbar toolbar = activity.findViewById(R.id.app_bar);
        toolbar.setTitle(title);
        //Set a Toolbar to act as the ActionBar for the Activity
        activity.setSupportActionBar(toolbar);
    }

    public static int getAvatarDrawable(String avatarPath){
        Log.i("MY", "avatar " + avatarPath );
        if(avatarPath.startsWith("b")){
            return R.drawable.boy_back_2;
        }else if(avatarPath.startsWith("g")){
            return R.drawable.girl_back_2;
        }else{
            return -1;
        }
    }
    public static int getFrontAvatarDrawable(String avatarPath){
        Log.i("MY", "avatar " + avatarPath );
        if(avatarPath.startsWith("b")){
            return R.drawable.boy_front_2;
        }else if(avatarPath.startsWith("g")){
            return R.drawable.girl_front_2;
        }else{
            return -1;
        }
    }

    public static Bitmap getImageBitmap(Activity activity, Uri currentPhotoUri){
        if(currentPhotoUri != null) {
            ContentResolver resolver = activity.getApplicationContext()
                    .getContentResolver();
            try {
                InputStream stream = resolver.openInputStream(currentPhotoUri);
                Bitmap bitmap = BitmapFactory.decodeStream(stream);
                Objects.requireNonNull(stream).close();
                return bitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void makeNoResultToast(Activity activity){
        Toast.makeText(activity, R.string.no_result, Toast.LENGTH_LONG).show();
    }

    public static void takePictureIntent(Activity activity){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    /**
     * Method called to save the image taken as a file in the gallery
     * @param bitmap the image taken
     * @throws IOException if there are some issue with the creation of the image file
     */
    public static Uri saveImage(Bitmap bitmap, Activity activity) throws IOException {
        // Create an image file name
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY).format(new Date());
        String name = "JPEG_" + timeStamp + "_.png";

        ContentResolver resolver = activity.getContentResolver();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name + ".jpg");
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
        Uri imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
        OutputStream fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));

        //for the jpeg quality, it goes from 0 to 100
        //for the png one, the quality is ignored
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        if (fos != null) {
            fos.close();
        }
        return  imageUri;
    }
}
