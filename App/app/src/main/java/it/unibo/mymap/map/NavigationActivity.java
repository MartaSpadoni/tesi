package it.unibo.mymap.map;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.mapbox.api.directions.v5.models.DirectionsRoute;

import it.unibo.mymap.R;
import it.unibo.mymap.utilities.Utility;

public class NavigationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_landmark);
        if (getIntent() != null) {
            Bundle b = getIntent().getBundleExtra("points");
            if (b != null) {
                DirectionsRoute route = DirectionsRoute.fromJson(b.getString("JSON"));
                if (route != null) {
                    Utility.replaceFragment(this, new NavigationFragment(route), NavigationFragment.class.getName());
                }
            } else {
                this.finish();
            }
        } else {
            this.finish();
        }

    }
}
