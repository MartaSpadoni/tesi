package it.unibo.mymap.landmark;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import it.unibo.mymap.MainActivity;
import it.unibo.mymap.R;
import it.unibo.mymap.database.Landmark;
import it.unibo.mymap.landmarkRecyclerView.LandmarkCardAdapter;
import it.unibo.mymap.landmarkRecyclerView.OnItemListener;
import it.unibo.mymap.utilities.MapUtility;
import it.unibo.mymap.utilities.Utility;

public class MyLandmarkFragment extends Fragment implements OnItemListener {

    private Activity activity;
    private RecyclerView recyclerView;
    private LandmarkCardAdapter adapter;
    private LandmarkViewModel model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.my_landmark_fragment, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_toolbar, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return true;
            }

        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
        if(activity != null){
            Utility.setUpToolbar((AppCompatActivity) activity, getString(R.string.my_landmark));
            setRecyclerView();
            model = new ViewModelProvider((ViewModelStoreOwner) activity).get(LandmarkViewModel.class);
            model.getLandmarkList().observe((LifecycleOwner) activity, new Observer<List<Landmark>>() {
                @Override
                public void onChanged(List<Landmark> landmarks) {
                    adapter.setData(landmarks);
                    if(activity.findViewById(R.id.localizeLandmarkButton) != null) {
                        if ((landmarks == null || landmarks.size() == 0)) {
                            activity.findViewById(R.id.localizeLandmarkButton).setVisibility(View.GONE);
                        } else {
                            activity.findViewById(R.id.localizeLandmarkButton).setVisibility(View.VISIBLE);
                        }
                    }
                }
            });

            activity.findViewById(R.id.localizeLandmarkButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(activity, MainActivity.class);
                    i.putExtra(MapUtility.LM_NEAR, true);
                    activity.startActivity(i);
                    activity.finish();
                }
            });

        }
    }

    private void setRecyclerView(){
        recyclerView = activity.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        adapter = new LandmarkCardAdapter(this, activity);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(int position) {
        Intent i = new Intent(activity, MainActivity.class);
        i.putExtra(MapUtility.LM_POS, this.adapter.getAbsolutPosition(position));
        activity.startActivity(i);
        activity.finish();
    }

    public void updateList(){
        adapter.updateList();
    }
}
