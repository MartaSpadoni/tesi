package it.unibo.mymap.map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import it.unibo.mymap.R;

public class PoiDialog extends DialogFragment {
    private PoiDialogListener listener;

    public PoiDialog(PoiDialogListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_choose_poi, null);
        setViewListener(view);
        return builder.setTitle(getString(R.string.poiDialogTitle)).setView(view).create();
    }


    private void setViewListener(View view){
        view.findViewById(R.id.museumContainer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onChoice(PoiType.TOURISM);
                dismiss();
            }
        });

        view.findViewById(R.id.fountainsContainer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onChoice(PoiType.FOUNTAINS);
                dismiss();
            }
        });

        view.findViewById(R.id.myLandContainer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onChoice(PoiType.MYLANDMARK);
                dismiss();
            }
        });

        view.findViewById(R.id.libraryContainer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onChoice(PoiType.LIBRARY);
                dismiss();
            }
        });
    }


    public interface PoiDialogListener {
        void onChoice(PoiType poiType);
    }

    public enum PoiType{
        MYLANDMARK, TOURISM, FOUNTAINS, LIBRARY;
    }
}
