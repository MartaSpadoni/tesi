package it.unibo.mymap.utilities;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.mapbox.geojson.BoundingBox;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.annotations.BubbleLayout;
import com.mapbox.mapboxsdk.geometry.LatLng;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.unibo.mymap.MainActivity;
import it.unibo.mymap.database.Landmark;
import it.unibo.mymap.map.MapFragment;
import it.unibo.mymap.R;

public class MapUtility {
    public static final String LM_NAME = "name";
    public static final String LM_DESC = "desc";
    public static final String LM_STATE = "selected";
    public static final String LM_PATH = "path";
    public static final String LM_POS = "landmarkPosition";
    public static final String LM_NEAR = "localizeNearMe";
    public static final String GC_OSM = "id";
    public static final String GC_NAME = "name";
    public static final String GC_STREET = "street";
    public static final String GC_CITY = "city";
    public static final String GC_HN = "hn";
    public static final String GC_COUNRTY = "country";
    public static final String OP_ID = "id";
    public static final String OP_NAME = "name";

    public enum InfoWindowType {
        LANDMARK, GEOCODING, OVERPASS;
    }

    /**
     * AsyncTask to generate Bitmap from Views to be used as iconImage in a SymbolLayer.
     * <p>
     * Call be optionally be called to update the underlying data source after execution.
     * </p>
     * <p>
     * Generating Views on background thread since we are not going to be adding them to the view hierarchy.
     * </p>
     */
    public static class GenerateViewIconTask extends AsyncTask<FeatureCollection, Void, HashMap<String, Bitmap>> {

        private final HashMap<String, View> viewMap = new HashMap<>();
        private final WeakReference<MainActivity> activityRef;
        private final boolean refreshSource;
        private final InfoWindowType type;

        public GenerateViewIconTask(MainActivity activity, boolean refreshSource, InfoWindowType type) {
            this.activityRef = new WeakReference<>(activity);
            this.refreshSource = refreshSource;
            this.type = type;
        }

        public GenerateViewIconTask(MainActivity activity) {
            this(activity, false, InfoWindowType.LANDMARK);
        }

        private String setLandmarkBubble(final BubbleLayout bubbleLayout, final Feature feature){
            String name = feature.getStringProperty(LM_NAME);
            String desc = feature.getStringProperty(LM_DESC);
            Uri uriImage = Uri.parse(feature.getStringProperty(LM_PATH));
            TextView titleTextView = bubbleLayout.findViewById(R.id.info_window_title);
            titleTextView.setText(name);
            TextView descriptionTextView = bubbleLayout.findViewById(R.id.info_window_description);
            descriptionTextView.setText(desc);
            if (!uriImage.toString().equals("")) {
                ((ImageView) bubbleLayout.findViewById(R.id.infoWindowImageView)).setImageBitmap(Utility.getImageBitmap(activityRef.get(), uriImage));
            } else {
                bubbleLayout.findViewById(R.id.infoWindowImageView).setVisibility(View.GONE);
            }
            bubbleLayout.findViewById(R.id.clickTextView).setVisibility(View.GONE);
            return name;
        }

        private String setGeocodingLocationBubble(final BubbleLayout bubbleLayout, final Feature feature){
            TextView titleTextView = bubbleLayout.findViewById(R.id.info_window_title);
            titleTextView.setText(feature.getStringProperty(GC_NAME));
            TextView descriptionTextView = bubbleLayout.findViewById(R.id.info_window_description);
            descriptionTextView.setText(feature.getStringProperty(GC_STREET) + " " + feature.getStringProperty(GC_HN)
                    + " " + feature.getStringProperty(GC_CITY) + " " + feature.getStringProperty(GC_COUNRTY));
            bubbleLayout.findViewById(R.id.infoWindowImageView).setVisibility(View.GONE);
            return feature.getStringProperty(GC_OSM);
        }

        private String setOverpassBubble(final BubbleLayout bubbleLayout, final Feature feature){
            Log.i("MY", "Feature in generate view: " + feature.toString());
            TextView titleTextView = bubbleLayout.findViewById(R.id.info_window_title);
            titleTextView.setText(feature.getStringProperty(OP_NAME));
            bubbleLayout.findViewById(R.id.info_window_description).setVisibility(View.GONE);
            bubbleLayout.findViewById(R.id.infoWindowImageView).setVisibility(View.GONE);
            return feature.getStringProperty(OP_ID);
        }
        @SuppressWarnings("WrongThread")
        @Override
        protected HashMap<String, Bitmap> doInBackground(FeatureCollection... params) {
            MainActivity activity = activityRef.get();
            if (activity != null) {
                HashMap<String, Bitmap> imagesMap = new HashMap<>();
                LayoutInflater inflater = LayoutInflater.from(activity);
                BubbleLayout bubbleLayout = (BubbleLayout)
                        inflater.inflate(R.layout.info_window_layout, null);
                String id = "";
                FeatureCollection featureCollection = params[0];

                for (Feature feature : featureCollection.features()) {
                    switch (type){
                        case LANDMARK:
                            id = setLandmarkBubble(bubbleLayout, feature);
                            break;
                        case GEOCODING:
                            id = setGeocodingLocationBubble(bubbleLayout, feature);
                            break;
                        case OVERPASS:
                            id = setOverpassBubble(bubbleLayout, feature);
                    }

                    int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                    bubbleLayout.measure(measureSpec, measureSpec);

                    float measuredWidth = bubbleLayout.getMeasuredWidth();

                    bubbleLayout.setArrowPosition(measuredWidth / 2 - 5);

                    Bitmap bitmap = SymbolGenerator.generate(bubbleLayout);
                    imagesMap.put(id, bitmap);
                    viewMap.put(id, bubbleLayout);
                }

                return imagesMap;
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(HashMap<String, Bitmap> bitmapHashMap) {
            super.onPostExecute(bitmapHashMap);
            MainActivity activity = activityRef.get();
            if (activity != null && bitmapHashMap != null) {
                MapFragment fragment = (MapFragment) activity.getSupportFragmentManager()
                        .findFragmentByTag(MapFragment.class.getName());
                if(fragment != null) {
                    fragment.setImageGenResults(bitmapHashMap);
                    if (refreshSource && type == InfoWindowType.LANDMARK) {
                        fragment.refreshSource();
                    }
                }
            }
        }
    }

    /**
     * Utility class to generate Bitmaps for Symbol.
     */
    private static class SymbolGenerator {
        /**
         * Generate a Bitmap from an Android SDK View.
         *
         * @param view the View to be drawn to a Bitmap
         * @return the generated bitmap
         */
        static Bitmap generate(@NonNull View view) {
            int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            view.measure(measureSpec, measureSpec);

            int measuredWidth = view.getMeasuredWidth();
            int measuredHeight = view.getMeasuredHeight();

            view.layout(0, 0, measuredWidth, measuredHeight);
            Bitmap bitmap = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888);
            bitmap.eraseColor(Color.TRANSPARENT);
            Canvas canvas = new Canvas(bitmap);
            view.draw(canvas);
            return bitmap;
        }
    }

    public static Feature getLandmarkFeature(Landmark l){
        Feature f = Feature.fromGeometry(Point.fromLngLat(l.getLng(), l.getLat()));
        f.addStringProperty(MapUtility.LM_NAME, l.getName());
        f.addStringProperty(MapUtility.LM_DESC, l.getDescription());
        f.addStringProperty(MapUtility.LM_PATH, l.getImagePath());
        f.addBooleanProperty(MapUtility.LM_STATE, false);
        return f;
    }

    public static Location getLocationFromLatLng(LatLng point){
        Location location = new Location("");
        location.setLongitude(point.getLongitude());
        location.setLatitude(point.getLatitude());
        return location;
    }

    public static LatLng getLatLongFromLocation(Location location){
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

    public static List<Point> trasformToPoints(FeatureCollection featureCollection){
        List<Point> points = new ArrayList<>();
        for(Feature f : featureCollection.features()){
            if(f.geometry() instanceof Point){
                points.add((Point)f.geometry());
            }
        }
        return points;
    }
    public static FeatureCollection findLandmarks(List<Landmark> list, FeatureCollection featureCollection){
        List<Point> points = trasformToPoints(featureCollection);
        List<Feature> landmarkFeatures = new ArrayList<>();
        for(Landmark l : list){
            if(points.contains(Point.fromLngLat(l.getLng(), l.getLat()))){
                Log.i("MY", "Landmark contenuto: " + l.getName());
                landmarkFeatures.add(getLandmarkFeature(l));
            }
        }

        return FeatureCollection.fromFeatures(landmarkFeatures);
    }

    public static BoundingBox fromTurfToMapbox(double[] bbox){
        if(bbox.length == 4) {
            Point first = Point.fromLngLat(bbox[0], bbox[1]);
            Point second = Point.fromLngLat(bbox[2], bbox[3]);
            return BoundingBox.fromPoints(first, second);
        }
        return null;
    }

}
