package it.unibo.mymap.landmarkRecyclerView;

/**
 * Interface to manage the listener for the click on an element of the RecyclerView
 */
public interface OnItemListener {
    void onItemClick(int position);
}