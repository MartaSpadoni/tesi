package it.unibo.mymap.map;

import android.app.Activity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.turf.TurfJoins;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.unibo.mymap.utilities.MapUtility;

public class APIRequestManager {

    private final static String TAG = "OVERPASS_REQUEST";
    private final static String FIRST_PART_PATH = "https://overpass-api.de/api/interpreter?data=[out:json];";
    private final static String LAST_PART_PATH = ";out%20meta;";
    private final static String OTM_PATH = "https://api.opentripmap.com/0.1/ru/places/bbox?";
    private final static String OTM_LAST = "&kinds=interesting_places&limit=100&format=geojson&apikey=5ae2e3f221c38a28845f05b61d14ca97b0f426a516b0dac1ecf8935e";
    private RequestQueue requestQueue;
    private Activity activity;
    private APICallback callback;
    private FeatureCollection searchArea;
    private Map<String, Map<String, String>> properties = new HashMap<>();


    public APIRequestManager(Activity activity, FeatureCollection area, APICallback apiCallback) {
        this.activity = activity;
        requestQueue = Volley.newRequestQueue(activity);
        callback = apiCallback;
        searchArea = area;
    }
    private String convertToOTMbbox(String bbox){
        List<String> bb = Arrays.asList(bbox
                .substring(1, bbox.length()-1)
                .split(","));
        return "lon_min="+bb.get(1)+"&lat_min="+bb.get(0)+"&lon_max="+bb.get(3)+"&lat_max="+bb.get(2)+"&";

    }

    public void findTourismFeature(String bbox){
        String query = OTM_PATH + convertToOTMbbox(bbox) + OTM_LAST;
        Log.i("MY", "OVERPASS request: " + query);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, query, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                FeatureCollection fc = FeatureCollection.fromJson(response.toString());
                fixFeature(fc);
                callback.onApiResponse(fc);
                Log.i("MY", response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("MY", error.toString());
                callback.onApiResponse(FeatureCollection.fromFeatures(new ArrayList<Feature>()));
            }
        });
        request.setTag(TAG);
        requestQueue.add(request);
    }

    private void fixFeature(FeatureCollection fc){
        List<Point> points = MapUtility.trasformToPoints(TurfJoins
                .pointsWithinPolygon(fc, searchArea));
        List<Feature> toRemove = new ArrayList<>();
        for(Feature f : fc.features()){
            if(points.contains((Point) f.geometry()) &&
                    f.hasProperty("name") &&
                    !f.getStringProperty("name").isEmpty()) {
                f.addBooleanProperty(MapUtility.LM_STATE, false);
                f.addStringProperty(MapUtility.OP_ID, f.id());
            }else{
                toRemove.add(f);
            }
        }
        fc.features().removeAll(toRemove);
    }


    /*public void findTourismFeature(String bbox){
        String query = FIRST_PART_PATH+getNodeTypeQueryPart("tourism", "")
                +getNOTNodeTypeQueryPart("tourism", "guest_house")
                +getNOTNodeTypeQueryPart("tourism", "hotel")
                +getNOTNodeTypeQueryPart("tourism", "hostel")
                + bbox+LAST_PART_PATH;
        String placeOfWorshipQuery = FIRST_PART_PATH+getNodeTypeQueryPart("amenity", "place_of_worship")+bbox+LAST_PART_PATH;

        Log.i("MY", "OVERPASS request: " + query);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, query, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    List<Feature> list = new ArrayList<>();
                    JSONArray a = response.getJSONArray("elements");
                    for(int i = 0; i < a.length(); i++){
                        JSONObject obj = a.getJSONObject(i);
                        Feature f = Feature.fromGeometry(Point.fromLngLat(obj.getDouble("lon"),obj.getDouble("lat")));
                        String key = getKey(obj.getDouble("lon"), obj.getDouble("lat"));
                        properties.put(key, new HashMap<String, String>());
                        try {
                            properties.get(key).put("name",obj.getJSONObject("tags").getString("name"));
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                        list.add(f);

                    }
                    fixResult(FeatureCollection.fromFeatures(list));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i("MY", response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("MY", error.toString());
                callback.onOverpassResponse(FeatureCollection.fromFeatures(new ArrayList<Feature>()));
            }
        });
        request.setTag(TAG);
        requestQueue.add(request);
    }*/

    public void findDrinkingFountains(String bbox){
       simpleOverpassRequest(bbox, "amenity", "drinking_water");
    }

    private void simpleOverpassRequest(String bbox, String s1, String s2){
            String query = FIRST_PART_PATH+getNodeTypeQueryPart(s1, s2)+ bbox+LAST_PART_PATH;
            Log.i("MY", "OVERPASS request: " + query);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, query, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        List<Feature> list = new ArrayList<>();
                        JSONArray a = response.getJSONArray("elements");
                        for(int i = 0; i < a.length(); i++){
                            JSONObject obj = a.getJSONObject(i);
                            Feature f = Feature.fromGeometry(Point.fromLngLat(obj.getDouble("lon"),obj.getDouble("lat")));
                            String key = getKey(obj.getDouble("lon"), obj.getDouble("lat"));
                            properties.put(key, new HashMap<String, String>());
                            try {
                                properties.get(key).put("name",obj.getJSONObject("tags").getString("name"));
                            }catch (JSONException e) {
                                e.printStackTrace();
                                //properties.get(key).put("name",activity.getString(R.string.no_more_info));
                            }
                            list.add(f);

                        }
                        fixResult(FeatureCollection.fromFeatures(list));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.i("MY", response.toString());

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("MY", error.toString());
                    callback.onApiResponse(FeatureCollection.fromFeatures(new ArrayList<Feature>()));
                }
            });
            request.setTag(TAG);
            requestQueue.add(request);
    }

    public void findLibrary(String bbox){
        simpleOverpassRequest(bbox, "amenity", "library");
    }


    public void cancelRequest(){
        if(requestQueue != null){
            requestQueue.cancelAll(TAG);
        }
    }

    private String getKey(Double lon, Double lat){
        return lon + "," + lat;
    }

    private void insertProperties(FeatureCollection fc){
        for(Feature f : fc.features()){
            String key = getKey(((Point)f.geometry()).longitude(), ((Point) f.geometry()).latitude());
            HashMap<String, String>featureProp = (HashMap<String, String>) properties
                    .get(key);
            f.addBooleanProperty(MapUtility.LM_STATE, false);
            f.addStringProperty(MapUtility.OP_ID, key);
            if(featureProp.get("name") != null &&
                    !featureProp.get("name").isEmpty()) {
                f.addStringProperty(MapUtility.OP_NAME, featureProp.get("name"));
            }
        }

    }



    private void fixResult(FeatureCollection fc){
        FeatureCollection pointsInSearchArea =
                TurfJoins.pointsWithinPolygon(fc,
                        searchArea);
        insertProperties(pointsInSearchArea);
        Log.i("MY", "after properties " + pointsInSearchArea.toJson());
        callback.onApiResponse(pointsInSearchArea);
    }

    private String getNodeTypeQueryPart(String s1, String s2){
        return "node[" + s1 + (s2.isEmpty() ? "]" : "=" + s2+"]");
    }

    private String getNOTNodeTypeQueryPart(String s1, String s2){
        return "[" + s1 + "!=" + s2+"]";
    }

    public interface APICallback {
        void onApiResponse(FeatureCollection fc);
    }
}
