package it.unibo.mymap.landmark;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import it.unibo.mymap.R;
import it.unibo.mymap.utilities.Utility;

import static it.unibo.mymap.utilities.Utility.REQUEST_IMAGE_CAPTURE;

public class EditLandmarkActivity extends AppCompatActivity {

    private Bitmap imageBitmap;
    private Uri currentPhotoUri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_landmark);
        if(savedInstanceState == null){
            Utility.replaceFragment(this, new EditLandmarkFragment(getIntent()
                    .getIntExtra("id_landmark", -1)),EditLandmarkFragment.class.getName());
        }else{
            currentPhotoUri = savedInstanceState.getParcelable("currentPhotoUri");
            imageBitmap = savedInstanceState.getParcelable("image");
            ((EditLandmarkFragment)this.getSupportFragmentManager()
                    .findFragmentByTag(EditLandmarkFragment.class.getName())).setUri(currentPhotoUri);
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_save:
                ((EditLandmarkFragment)this.getSupportFragmentManager()
                        .findFragmentByTag(EditLandmarkFragment.class.getName())).saveUpdate();
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");
                try {
                    if (imageBitmap != null) {
                        //method to save the image in the gallery of the device
                        currentPhotoUri = Utility.saveImage(imageBitmap, this);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ((EditLandmarkFragment)this.getSupportFragmentManager()
                        .findFragmentByTag(EditLandmarkFragment.class.getName())).setUri(currentPhotoUri);
            }

        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //save the bitmap and the uri of the image taken
        outState.putParcelable("image", imageBitmap);
        outState.putParcelable("currentPhotoUri", currentPhotoUri);
        super.onSaveInstanceState(outState);
    }
}
