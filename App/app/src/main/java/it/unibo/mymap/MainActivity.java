package it.unibo.mymap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;

import java.io.IOException;
import java.util.List;

import it.unibo.mymap.account.AccountFragment;
import it.unibo.mymap.account.RegistrationActivity;
import it.unibo.mymap.landmark.AddLandmarkDialog;
import it.unibo.mymap.landmark.MyLandmarkFragment;
import it.unibo.mymap.map.MapFragment;
import it.unibo.mymap.utilities.InternetUtilities;
import it.unibo.mymap.utilities.MapUtility;
import it.unibo.mymap.utilities.Utility;

import static com.mapbox.mapboxsdk.style.expressions.Expression.eq;
import static com.mapbox.mapboxsdk.style.expressions.Expression.get;
import static com.mapbox.mapboxsdk.style.expressions.Expression.literal;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAnchor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconOffset;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconSize;
import static it.unibo.mymap.utilities.Utility.CHANGE_AVATAR;
import static it.unibo.mymap.utilities.Utility.NAVIGATION_SESSION;
import static it.unibo.mymap.utilities.Utility.REGISTRATION;
import static it.unibo.mymap.utilities.Utility.REQUEST_IMAGE_CAPTURE;
import static it.unibo.mymap.utilities.Utility.RESULT_NAVIGATION_END;
import static it.unibo.mymap.utilities.Utility.replaceFragment;

public class MainActivity extends AppCompatActivity implements PermissionsListener{

    private SharedPreferences sharedPreferences;
    private PermissionsManager permissionsManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpBottomNavigationView();
        permissionsManager = new PermissionsManager(this);
        sharedPreferences = getSharedPreferences(getString(R.string.settings_file), Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean(getString(R.string.first_access), true)) {
            startActivityForResult(new Intent(this, RegistrationActivity.class), REGISTRATION);
        } else if(savedInstanceState == null){
            if (PermissionsManager.areLocationPermissionsGranted(this)) {
                if (getIntent() != null) {
                    replaceFragment(this, new MapFragment(getIntent().getIntExtra(MapUtility.LM_POS, -1),
                            getIntent().getBooleanExtra(MapUtility.LM_NEAR, false)), MapFragment.class.getName());
                } else {
                    replaceFragment(this, new MapFragment(-1, false), MapFragment.class.getName());
                }
            } else {
                requestLocationPermission();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        InternetUtilities.registerNetworkCallback(this);
        InternetUtilities.makeSnackbar(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            //unregistered the callback
            connectivityManager.unregisterNetworkCallback(InternetUtilities.getNetworkCallback());
        }
    }

    public void requestLocationPermission(){
        Log.i("MY", "richiedo permessi");
        permissionsManager.requestLocationPermissions(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("MY", "on activity result " + requestCode);
        switch (requestCode){
            case REGISTRATION:
                if(resultCode == RESULT_OK){
                    if(InternetUtilities.getIsNetworkConnected()) {
                        replaceFragment(this, new MapFragment(-1, false), MapFragment.class.getName());
                    }else{
                        InternetUtilities.makeSnackbar(this);
                        InternetUtilities.getSnackbar().show();
                    }
                }
                break;
            case REQUEST_IMAGE_CAPTURE:
                if(resultCode == RESULT_OK){
                    Bundle extras = data.getExtras();
                    Uri currentPhotoUri = null;
                    if (extras != null) {
                        Bitmap imageBitmap = (Bitmap) extras.get("data");

                        try {
                            if (imageBitmap != null) {
                                //method to save the image in the gallery of the device
                                currentPhotoUri = Utility.saveImage(imageBitmap, this);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    // Load a specific media item, and show it in the ImageView
                    Bitmap bitmap = Utility.getImageBitmap(this, currentPhotoUri);
                    if (bitmap != null){
                        AddLandmarkDialog dialog = (AddLandmarkDialog) getSupportFragmentManager().findFragmentByTag(AddLandmarkDialog.class.getName());
                        if(dialog != null) {
                            dialog.setImageView(bitmap);
                            dialog.setPhotoUri(currentPhotoUri);
                        }
                    }
                }
                break;
            case CHANGE_AVATAR:
                Log.i("MY", "on activity result case");
                if(resultCode == RESULT_OK){
                    AccountFragment accountFragment = (AccountFragment)getSupportFragmentManager()
                            .findFragmentByTag(AccountFragment.class.getName());
                    if(accountFragment != null){
                        Log.i("MY", "change finito aggiorno");
                        SharedPreferences sp = getSharedPreferences(getString(R.string.settings_file), MODE_PRIVATE);
                        accountFragment.setAvatarImage(sp);
                    }
                }
                break;
            case NAVIGATION_SESSION:
                Log.i("MY", "guardo se finita");
                MapFragment f = ((MapFragment)getSupportFragmentManager().findFragmentByTag(MapFragment.class.getName()));
                f.onNavigationEnd();
                if(resultCode == RESULT_NAVIGATION_END){
                    f.showEcoFeedback();
                }

                break;
            default:
                Log.i("MY", "DEFAULT");
        }
    }

    private void setUpBottomNavigationView(){
        final AppCompatActivity activity = this;
        ((BottomNavigationView) findViewById(R.id.bottomNav)).setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.map_view:
                        replaceFragment(activity, new MapFragment(-1, false), MapFragment.class.getName());
                        break;
                    case R.id.account:
                        replaceFragment(activity, new AccountFragment(), AccountFragment.class.getName());
                        break;
                    case R.id.my_landmark:
                        replaceFragment(activity, new MyLandmarkFragment(), MyLandmarkFragment.class.getName());
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        MapFragment mapFragment = (MapFragment) getSupportFragmentManager()
                .findFragmentByTag(MapFragment.class.getName());
        if(mapFragment == null || mapFragment.onBackPressed()){
            super.onBackPressed();
        }
    }

    @Override
    public void onPermissionResult(boolean granted) {
        Log.i("MY", "Risultato richiesta" + granted);
        if(granted){
            ((MapFragment)getSupportFragmentManager()
                    .findFragmentByTag(MapFragment.class.getName()))
                    .setUpMap();
        }else{
            this.finish();
        }
    }
}
