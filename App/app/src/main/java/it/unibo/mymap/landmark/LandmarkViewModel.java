package it.unibo.mymap.landmark;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import it.unibo.mymap.database.Landmark;
import it.unibo.mymap.database.MapRepository;

public class LandmarkViewModel extends AndroidViewModel {
    private MapRepository repository;
    private LiveData<List<Landmark>> landmarkList;

    public LandmarkViewModel(@NonNull Application application) {
        super(application);
        repository = new MapRepository(application);
        landmarkList = repository.getLandmarkList();
    }

    public LiveData<List<Landmark>> getLandmarkList() {
        return landmarkList;
    }

    public Landmark getLandmark(int id){
        return repository.getLandmark(id);
    }

    public void updateLandmark(Landmark l){ repository.updateLandmark(l);}

    public void deleteLandmark(Landmark l){
        repository.deleteLandmark(l);
    }


    public void addLandmark(Landmark l){
        this.repository.addLandmark(l);
    }
}
