package it.unibo.mymap.landmark;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

import it.unibo.mymap.R;
import it.unibo.mymap.database.Landmark;
import it.unibo.mymap.utilities.Utility;

public class EditLandmarkFragment extends Fragment {

    private final int landmarkId;
    private Activity activity;
    private LandmarkViewModel landmarkViewModel;
    private Landmark landmark;
    private TextInputEditText placeName;
    private TextInputEditText placeDescription;
    private ImageView imageView;
    private Uri imagePath;

    public EditLandmarkFragment(int id_landmark) {
        landmarkId = id_landmark;
        Log.i("MY", "landmark id: " + landmarkId);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_edit_landmark, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
        if(activity != null){
            Utility.setUpToolbar((AppCompatActivity) activity, getString(R.string.edit_landmark));
            ((AppCompatActivity) activity).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) activity).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_close_24);
            imageView = ((ImageView)activity.findViewById(R.id.landmarkImageView));
            placeName = ((TextInputEditText)activity.findViewById(R.id.placeNameEditText));
            placeDescription = ((TextInputEditText)activity.findViewById(R.id.placeDescriptionEditText));
            landmarkViewModel = new ViewModelProvider((ViewModelStoreOwner) activity).get(LandmarkViewModel.class);
            landmarkViewModel.getLandmarkList().observe((LifecycleOwner) activity, new Observer<List<Landmark>>() {
                @Override
                public void onChanged(List<Landmark> landmarks) {
                    landmark = findLandmarkInList(landmarks);
                    if(landmark != null && activity != null ){
                        if(!landmark.getImagePath().isEmpty()) {
                            imageView.setImageBitmap(Utility.getImageBitmap(activity, Uri.parse(landmark.getImagePath())));
                        }
                        placeName.setText(landmark.getName());
                        placeDescription.setText(landmark.getDescription());
                    }
                }
            });

            setOnClickListener();
        }
    }

    private void setOnClickListener(){
        activity.findViewById(R.id.takeImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.takePictureIntent(activity);
            }
        });

    }

    public void setUri(Uri imagePath){
        landmark.setImagePath(imagePath.toString());
        imageView.setImageBitmap(Utility.getImageBitmap(activity, imagePath));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.edit_menu, menu);
    }

    private Landmark findLandmarkInList(List<Landmark> landmarkList){
        for(Landmark l : landmarkList){
            if(l.getId() == landmarkId){
                return l;
            }
        }
        return null;
    }

    public void saveUpdate(){
        landmark.setName(placeName.getText().toString());
        landmark.setDescription(placeDescription.getText().toString());
        landmarkViewModel.updateLandmark(landmark);
        activity.finish();
    }

}
