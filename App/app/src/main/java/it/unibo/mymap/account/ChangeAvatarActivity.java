package it.unibo.mymap.account;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import it.unibo.mymap.R;
import it.unibo.mymap.utilities.Utility;

public class ChangeAvatarActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_landmark);
        Utility.replaceFragment(this, new ChooseAvatarFragment(), ChooseAvatarFragment.class.getName());
    }
}
