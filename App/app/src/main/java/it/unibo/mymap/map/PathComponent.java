package it.unibo.mymap.map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import java.util.List;

import it.unibo.mymap.R;
import it.unibo.mymap.utilities.InternetUtilities;
import it.unibo.mymap.utilities.Utility;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static it.unibo.mymap.utilities.MapUtility.getLocationFromLatLng;

public class PathComponent implements Callback<DirectionsResponse> {

    private FragmentActivity activity;
    private MapFragment mapFragment;
    private Button navigationButton;
    private Button directionsButton;
    private TextView toTextView;
    private LatLng destination;
    private LatLng origin;
    private View component;
    private DirectionsRoute routeToNavigate;
    private RouteListener routeListener;

    public PathComponent(FragmentActivity activity, RouteListener listener) {
        this.activity = activity;
        this.routeListener = listener;
        setComponent();
    }

    private void setComponent(){
        if(activity != null){
            mapFragment = (MapFragment) activity.getSupportFragmentManager().findFragmentByTag(MapFragment.class.getName());
            if(mapFragment != null) {
                Log.i("MY", "setto il component");
                component = activity.findViewById(R.id.PathComponent);
                navigationButton = activity.findViewById(R.id.navigationButton);
                directionsButton = activity.findViewById(R.id.directionsButton);
                toTextView = activity.findViewById(R.id.toTextView);
                if(directionsButton != null && navigationButton != null)
                setOnClickListener();
            }
        }
    }

    private Point getPointFromLatLng(LatLng p){
        return Point.fromLngLat(p.getLongitude(), p.getLatitude());
    }

    private void setOnClickListener(){
        directionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                directionsButton.setVisibility(View.INVISIBLE);
                Log.i("MY", "Nell listener di direction button");
                if(getLocationFromLatLng(origin).distanceTo(getLocationFromLatLng(destination)) > 3000f){
                    SharedPreferences sp = activity.getSharedPreferences(activity.getString(R.string.settings_file), MODE_PRIVATE);
                    new AlertDialog.Builder(activity).setTitle("Luogo lontano")
                            .setMessage(sp.getString(activity.getString(R.string.username),"")+" il luogo dove vuoi andare è lontano, " +
                                    "chiedi ad un adulto di accompagnarti")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mapFragment.showLoading();
                                    findRoute(activity, getPointFromLatLng(origin),
                                            getPointFromLatLng(destination));
                                }
                            }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create().show();
                }else{
                    mapFragment.showLoading();
                    findRoute(activity, getPointFromLatLng(origin),
                            getPointFromLatLng(destination));
                }
            }
        });

        navigationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(routeToNavigate != null) {
                    Bundle b = new Bundle();
                    b.putDouble("latOrigin",origin.getLatitude());
                    b.putDouble("longOrigin",origin.getLongitude());
                    b.putDouble("latDest",destination.getLatitude());
                    b.putDouble("longDest",destination.getLongitude());
                    b.putString("JSON", routeToNavigate.toJson());
                    Intent i = new Intent(activity, NavigationActivity.class);
                    i.putExtra("points", b);
                    activity.startActivityForResult(i, Utility.NAVIGATION_SESSION);
                }
            }
        });

    }

    public void updateOrigin(LatLng origin){
        this.origin = origin;
    }

    public void setComponentVisibility(int visibility){
        setComponent();
        this.component.setVisibility(visibility);
    }

    public void setNewDestination(String destinationName, LatLng destination, LatLng origin){
        toTextView.setText(destinationName);
        this.destination = destination;
        this.origin = origin;
        this.directionsButton.setVisibility(View.VISIBLE);
        this.navigationButton.setVisibility(View.INVISIBLE);
    }

    private void findRoute(Activity activity, Point origin, Point destination) {
        if(InternetUtilities.getIsNetworkConnected()) {
            Log.i("MY", "In find route del path");
            NavigationRoute.builder(activity)
                    .accessToken("pk." + activity.getString(R.string.gh_key))
                    .baseUrl(activity.getString(R.string.base_url))
                    .user("gh")
                    .profile("foot")
                    .origin(origin)
                    .destination(destination)
                    .alternatives(true)
                    .build()
                    .getRoute(this);
        }else{
            InternetUtilities.getSnackbar().show();
        }
    }

    @Override
    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
        if (response.isSuccessful()
                && response.body() != null
                && !response.body().routes().isEmpty()) {
            Log.i("MY", "route response");
            mapFragment.hideLoading();
            List<DirectionsRoute> routes = response.body().routes();
            routeToNavigate = routes.get(0);
            routeListener.onNewRoute(routeToNavigate);
            navigationButton.setVisibility(View.VISIBLE);

        }else{
            Log.i("MY", "Problemi con il routing");
        }
    }
    @Override
    public void onFailure(Call<DirectionsResponse> call, Throwable t) {
        Toast.makeText(activity, R.string.route_not_found, Toast.LENGTH_LONG ).show();
    }

    public interface RouteListener{
        void onNewRoute(DirectionsRoute route);
    }
}
