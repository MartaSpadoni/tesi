package it.unibo.mymap.geocoding;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.graphhopper.directions.api.client.ApiException;
import com.graphhopper.directions.api.client.api.GeocodingApi;
import com.graphhopper.directions.api.client.model.GeocodingLocation;
import com.graphhopper.directions.api.client.model.GeocodingPoint;
import com.graphhopper.directions.api.client.model.GeocodingResponse;
import com.mapbox.mapboxsdk.geometry.LatLng;

import java.util.ArrayList;
import java.util.List;

import it.unibo.mymap.utilities.MapUtility;

public class FetchGeocodingTask extends AsyncTask<FetchGeocodingConfig, Void, List<GeocodingLocation>> {

    private final String ghKey;
    private final FetchGeocodingTaskCallbackInterface callbackInterface;
    private final LatLng point;

    public FetchGeocodingTask(FetchGeocodingTaskCallbackInterface callbackInterface, String ghKey, LatLng point) {
        this.callbackInterface = callbackInterface;
        this.ghKey = ghKey;
        this.point = point;
    }

    private List<GeocodingLocation> checkReverseGeocodingResults(LatLng point, List<GeocodingLocation> results){
        List<GeocodingLocation> checkedList = new ArrayList<>();
        for(GeocodingLocation l : results){
            if(MapUtility.getLocationFromLatLng(point)
                    .distanceTo(getLocationFromGeocodingPoint(l.getPoint())) < 25){
                checkedList.add(l);
            }
        }
        return checkedList;
    }

    private Location getLocationFromGeocodingPoint(GeocodingPoint gp){
        LatLng point = new LatLng();
        point.setLongitude(gp.getLng());
        point.setLatitude(gp.getLat());
        return MapUtility.getLocationFromLatLng(point);
    }

    @Override
    protected List<GeocodingLocation> doInBackground(FetchGeocodingConfig... geocodingConfigs) {

        if (geocodingConfigs.length != 1)
            throw new IllegalArgumentException("It's only possible to fetch one geocoding at a time");

        List<GeocodingLocation> locations = new ArrayList<>();
        GeocodingApi api = new GeocodingApi();

        try {
            FetchGeocodingConfig geocodingConfig = geocodingConfigs[0];
            GeocodingResponse res = api.geocodeGet(ghKey, geocodingConfig.query, geocodingConfig.locale, geocodingConfig.limit, geocodingConfig.reverse, geocodingConfig.point, geocodingConfig.provider);
            locations = res.getHits();

            if (locations.isEmpty()) {
                //callbackInterface.onError(R.string.error_location_not_found);
            }

        } catch (ApiException e) {
            //callbackInterface.onError(R.string.error_fetching_geocoding);
            Log.i("MY", "An exception occured when fetching geocoding results for" + geocodingConfigs[0].query);
        }

        return locations;
    }

    @Override
    protected void onPostExecute(List<GeocodingLocation> locations) {
        if(point != null){
            callbackInterface.onPostExecuteGeocodingSearch(checkReverseGeocodingResults(point, locations));
        }else {
            callbackInterface.onPostExecuteGeocodingSearch(locations);
        }
    }
}