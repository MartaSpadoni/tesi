package it.unibo.mymap.geocoding;

import com.graphhopper.directions.api.client.model.GeocodingLocation;

import java.util.List;

public interface FetchGeocodingTaskCallbackInterface {

    void onError(String errore);

    void onPostExecuteGeocodingSearch(List<GeocodingLocation> points);

}
