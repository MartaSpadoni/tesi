package it.unibo.mymap.map;

import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import com.mapbox.android.core.location.LocationEngineCallback;
import com.mapbox.android.core.location.LocationEngineResult;

public class MyLocationEngineCallback implements LocationEngineCallback<LocationEngineResult> {

    private final FragmentActivity activity;

    public MyLocationEngineCallback(FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onSuccess(LocationEngineResult result) {

        if(activity != null && result.getLastLocation() != null){
           MapFragment f = (MapFragment) activity.getSupportFragmentManager()
                   .findFragmentByTag(MapFragment.class.getName());
           if(f != null){
               f.updateLocationComponent(result.getLastLocation());
           }
        }
    }

    @Override
    public void onFailure(@NonNull Exception exception) {
        Log.i("MY", "failure location engine");
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        activity.startActivity(
                                new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
