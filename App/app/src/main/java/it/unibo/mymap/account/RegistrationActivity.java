package it.unibo.mymap.account;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.imageview.ShapeableImageView;

import java.io.IOException;

import it.unibo.mymap.R;
import it.unibo.mymap.utilities.Utility;

import static it.unibo.mymap.utilities.Utility.REQUEST_IMAGE_CAPTURE;

public class RegistrationActivity extends AppCompatActivity {

    private Uri currentPhotoUri;
    private Bitmap imageBitmap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        if(savedInstanceState == null){
            Utility.replaceFragment(this, new RegistrationFragment(), RegistrationFragment.class.getName());
        }else{
            currentPhotoUri = savedInstanceState.getParcelable("currentPhotoUri");
            imageBitmap = savedInstanceState.getParcelable("image");
            RegistrationFragment fragment = (RegistrationFragment) getSupportFragmentManager().findFragmentByTag(RegistrationFragment.class.getName());
            if (fragment != null) {
                fragment.setImageView(imageBitmap);
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                imageBitmap = (Bitmap) extras.get("data");
                try {
                    if (imageBitmap != null) {
                        //method to save the image in the gallery of the device
                        currentPhotoUri = Utility.saveImage(imageBitmap, this);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Log.d("LAB", String.valueOf(currentPhotoUri));
            // Load a specific media item, and show it in the ImageView
            Bitmap bitmap = Utility.getImageBitmap(this, currentPhotoUri);
            if (bitmap != null){
                ShapeableImageView imageView = findViewById(R.id.imageView);
                imageView.setImageBitmap(bitmap);
            }
        }
    }



    //get the URI of the photo
    Uri getCurrentPhotoUri(){
        return currentPhotoUri;
    }

    /**
     * Method called before a configuration change
     * @param outState bundle where saved elements are stored
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //save the bitmap and the uri of the image taken
        outState.putParcelable("image", imageBitmap);
        outState.putParcelable("currentPhotoUri", currentPhotoUri);
        super.onSaveInstanceState(outState);
    }
}
